package com.senchuk.carrentservice.common.entity;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.Instant;

public class AuditingEntityListener {

    @PrePersist
    public void prePersist(final AbstractEntity abstractEntity) {
        abstractEntity.setCreatedAt(Instant.now());
    }

    @PreUpdate
    public void preUpdate(final AbstractEntity abstractEntity) {
        abstractEntity.setUpdatedAt(Instant.now());
    }

}


package com.senchuk.carrentservice.service;

import com.senchuk.carrentservice.model.Vehicle;
import com.senchuk.carrentservice.model.VehicleUpdate;

import java.util.List;
import java.util.UUID;

public interface VehicleService {

    Vehicle create(final Vehicle vehicle);

    Vehicle update(final VehicleUpdate vehicleUpdate);

    void delete(final UUID id);

    List<Vehicle> getAll();

    Vehicle getById(final UUID id);

    void discount();

}

package com.senchuk.carrentservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Log4j
@Service
@RequiredArgsConstructor
public class VehicleScheduleService {

    private final VehicleService vehicleService;

    @Scheduled(cron = "00 00 00 * * ?")
    private void discount() {
        vehicleService.discount();
        log.info("Discount vehicle method calles");
    }

}

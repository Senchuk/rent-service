package com.senchuk.carrentservice.service;

import com.senchuk.carrentservice.model.Order;
import com.senchuk.carrentservice.model.OrderBill;
import com.senchuk.carrentservice.model.OrderCreate;

import java.util.List;
import java.util.UUID;

public interface OrderService {

    Order create(final OrderCreate orderCreate);

    Order getById(final UUID id);

    List<Order> getAllByCurrentUser();

    OrderBill getBillById(final UUID id);

    Order endRent(final UUID id);

    Order pay(final UUID id);

    List<Order> getAllByVehicleId(final UUID vehicleId, final boolean isPast);

    List<Order> getAllByIsPaid(final Boolean isPaid);

}

package com.senchuk.carrentservice.service;

import com.senchuk.carrentservice.model.AuthRequest;
import com.senchuk.carrentservice.model.User;

import java.util.List;
import java.util.UUID;

public interface UserService {

    User create(final AuthRequest authRequest);

    List<User> getAll();

    User getCurrent();

    User getByEmail(final String email);

    User getById(final UUID id);

    Boolean isExistByEmail(final String email);

}

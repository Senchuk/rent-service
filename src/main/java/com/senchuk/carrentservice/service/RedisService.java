package com.senchuk.carrentservice.service;

import com.senchuk.carrentservice.model.TokenResponse;

import java.util.UUID;


public interface RedisService {

    void save(final UUID userId, final TokenResponse value);

    void delete(final UUID userId, final TokenResponse refreshToken);

    void deleteAll(final UUID userId);

    boolean checkToken(final UUID userId, final String refreshToken);

    boolean isExists(final UUID id);

}

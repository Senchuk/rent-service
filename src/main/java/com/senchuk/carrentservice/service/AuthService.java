package com.senchuk.carrentservice.service;

import com.senchuk.carrentservice.model.*;

import java.util.UUID;


public interface AuthService {

    TokenResponse signUp(final AuthRequest authRequest);

    TokenResponse signIn(final AuthRequest authRequest);

    TokenResponse refreshToken(final String refreshToken);

    void signOut(final TokenResponse tokenResponse);

    void signOutAll();

    void signOutAllByUserId(final UUID userId);

}

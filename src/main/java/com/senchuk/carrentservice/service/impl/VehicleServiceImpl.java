package com.senchuk.carrentservice.service.impl;

import com.senchuk.carrentservice.exception.EntityNotFoundException;
import com.senchuk.carrentservice.exception.InvalidRequestParamException;
import com.senchuk.carrentservice.exception.VehicleIsAlreadyExistException;
import com.senchuk.carrentservice.model.Vehicle;
import com.senchuk.carrentservice.model.VehicleUpdate;
import com.senchuk.carrentservice.repositoty.VehicleRepository;
import com.senchuk.carrentservice.service.VehicleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.senchuk.carrentservice.mapper.VehicleMapper.VEHICLE_MAPPER;

@Log4j
@Service
@RequiredArgsConstructor
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;

    @Override
    public Vehicle create(final Vehicle vehicle) {
        vehicleRepository.findByNumber(vehicle.getNumber()).ifPresent(value -> {
            throw new VehicleIsAlreadyExistException(vehicle.getNumber());
        });
        if (vehicle.getAge().isAfter(LocalDate.now())) {
            throw new InvalidRequestParamException("Vehicle age cannot be after then now");
        }
        var vehicleEntity = VEHICLE_MAPPER.toEntity(vehicle);
        var savedVehicleEntity = vehicleRepository.save(vehicleEntity);
        log.info("Create vehicle method completed");
        return VEHICLE_MAPPER.toModel(savedVehicleEntity);

    }

    @Override
    public Vehicle update(final VehicleUpdate vehicleUpdate) {
        var vehicle = getById(vehicleUpdate.getId());
        if (vehicleUpdate.getDescription() != null) {
            vehicle.setDescription(vehicleUpdate.getDescription());
        }
        if (vehicleUpdate.getQuality() != null) {
            vehicle.setQuality(vehicleUpdate.getQuality());
        }
        if (vehicleUpdate.getPricePerDay() != null) {
            vehicle.setPricePerDay(vehicleUpdate.getPricePerDay());
        }
        if (vehicleUpdate.getDelayPricePerHour() != null) {
            vehicle.setDelayPricePerHour(vehicleUpdate.getDelayPricePerHour());
        }
        if (vehicleUpdate.getOperationalDiscount() != null) {
            vehicle.setOperationalDiscount(vehicleUpdate.getOperationalDiscount());
        }
        var vehicleEntity = VEHICLE_MAPPER.toEntity(vehicle);
        var savedVehicleEntity = vehicleRepository.save(vehicleEntity);
        log.info("Update vehicle method completed");
        return VEHICLE_MAPPER.toModel(savedVehicleEntity);
    }

    @Override
    public void delete(final UUID id) {
        var vehicle = getById(id);
        vehicle.setDeletedAt(Instant.now());
        vehicleRepository.save(VEHICLE_MAPPER.toEntity(vehicle));
        log.info("Delete vehicle method completed");
    }

    @Override
    public List<Vehicle> getAll() {
        var vehicles =  vehicleRepository.findAll().stream().map(VEHICLE_MAPPER::toModel).collect(Collectors.toList());
        log.info("Get all vehicles method completed");
        return vehicles;
    }

    @Override
    public Vehicle getById(final UUID id) {
        var vehicleEntity =  vehicleRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException(String.format("Vehicle with such id %s not found", id));
        });
        log.info("Get vehicle by id method completed");
        return VEHICLE_MAPPER.toModel(vehicleEntity);
    }

    @Override
    public void discount() {
        vehicleRepository.findAllByAge(LocalDate.now()).forEach(vehicle -> {
            var priceWithDiscount = vehicle.getPricePerDay() - vehicle.getOperationalDiscount();
            if (priceWithDiscount > 0 ) {
                vehicle.setPricePerDay(priceWithDiscount);
                vehicleRepository.save(vehicle);
            }
        });
        log.info("Discount vehicle method completed");
    }

}

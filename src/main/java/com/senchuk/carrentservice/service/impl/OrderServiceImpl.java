package com.senchuk.carrentservice.service.impl;

import com.senchuk.carrentservice.entity.OrderEntity;
import com.senchuk.carrentservice.exception.EntityNotFoundException;
import com.senchuk.carrentservice.exception.InvalidRequestParamException;
import com.senchuk.carrentservice.model.Order;
import com.senchuk.carrentservice.model.OrderBill;
import com.senchuk.carrentservice.model.OrderCreate;
import com.senchuk.carrentservice.repositoty.OrderRepository;
import com.senchuk.carrentservice.service.OrderService;
import com.senchuk.carrentservice.service.UserService;
import com.senchuk.carrentservice.service.VehicleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.senchuk.carrentservice.mapper.OrderMapper.ORDER_MAPPER;
import static com.senchuk.carrentservice.mapper.UserMapper.USER_MAPPER;
import static com.senchuk.carrentservice.mapper.VehicleMapper.VEHICLE_MAPPER;
import static com.senchuk.carrentservice.utils.SecurityUtils.getCurrentUser;


@Log4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final VehicleService vehicleService;
    private final UserService userService;

    private final OrderRepository orderRepository;

    @Override
    public Order create(final OrderCreate orderCreate) {
        var vehicle = vehicleService.getById(orderCreate.getVehicleId());
        var user = userService.getCurrent();
        if (orderCreate.getRentStartTime().isBefore(Instant.now()) && orderCreate.getRentStartTime().equals(Instant.now())) {
            throw new InvalidRequestParamException(
                    String.format("You cannot start renting in %s, because this date has passed", orderCreate.getRentStartTime()));
        }
        var orderEntity = ORDER_MAPPER.toEntity(orderCreate);
        orderEntity.setUser(USER_MAPPER.toEntity(user));
        orderEntity.setVehicle(VEHICLE_MAPPER.toEntity(vehicle));
        orderEntity.setDuration(orderCreate.getDuration());
        orderEntity.setPricePerDay(vehicle.getPricePerDay());
        orderEntity.setDelayPricePerHour(vehicle.getDelayPricePerHour());
        var savedOrderEntity = orderRepository.save(orderEntity);
        log.info("Create order method completed");
        return ORDER_MAPPER.toModel(savedOrderEntity);
    }

    @Override
    public Order getById(final UUID id) {
        var orderEntity = orderRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException(String.format("Order with such id %s is not exist", id));
        });
        log.info("Get order by order id in method completed");
        return ORDER_MAPPER.toModel(orderEntity);
    }

    @Override
    public List<Order> getAllByCurrentUser() {
        var user = getCurrentUser();
        var orderEntities = orderRepository.findByUserId(user.getId());
        log.info("Get all current users' orders method completed");
        return orderEntities.stream()
                .map(ORDER_MAPPER::toModel)
                .collect(Collectors.toList());
    }

    @Override
    public OrderBill getBillById(final UUID id) {
        var order = getById(id);
        log.info("Get order bill method completed");
        return ORDER_MAPPER.toOrderBill(order);
    }

    @Override
    public Order endRent(final UUID id) {
        var orderEntity = orderRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException(String.format("Order with such id %s is not exist", id));
        });
        if (orderEntity.getRentCompletionTime() != null) {
            throw new InvalidRequestParamException("This rental is already finished!");
        }
        orderEntity.setRentCompletionTime(Instant.now());
        var bill = getBill(orderEntity);
        orderEntity.setTotalCost(bill);
        var savedOrderEntity = orderRepository.save(orderEntity);
        log.info("End rental method completed");
        return ORDER_MAPPER.toModel(savedOrderEntity);
    }

    @Override
    public Order pay(final UUID id) {
        var orderEntity = orderRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException(String.format("Order with such id %s is not exist", id));
        });
        if (orderEntity.getRentCompletionTime() == null) {
            throw new InvalidRequestParamException("This rental has not finished yet!");
        }
        if (orderEntity.getIsPaid() != null) {
            throw new InvalidRequestParamException("This rental has already been paid!");
        }
        orderEntity.setIsPaid(true);
        var savedOrderEntity = orderRepository.save(orderEntity);
        log.info("Pay for order method completed");
        return ORDER_MAPPER.toModel(savedOrderEntity);
    }

    @Override
    public List<Order> getAllByVehicleId(final UUID vehicleId, final boolean isPast) {
        var orders =  orderRepository.findAllByVehicleId(vehicleId, isPast, Instant.now())
                .stream()
                .map(ORDER_MAPPER::toModel)
                .collect(Collectors.toList());
        log.info("Get all vehicles method completed");
        return orders;
    }

    @Override
    public List<Order> getAllByIsPaid(final Boolean isPaid) {
        var orders =  orderRepository.findAll(isPaid).stream().map(ORDER_MAPPER::toModel).collect(Collectors.toList());
        log.info("Get all vehicles method completed");
        return orders;
    }

    private Float getBill(final OrderEntity orderEntity) {
        var plannedEndTime = orderEntity.getRentStartTime().plus(Duration.ofDays(orderEntity.getDuration()));
        var totalCost = orderEntity.getPricePerDay() * orderEntity.getDuration();
        if (plannedEndTime.isBefore(orderEntity.getRentCompletionTime())) {
            var delay = Duration.between(orderEntity.getRentCompletionTime(), plannedEndTime).toHours();
            var delayCost = orderEntity.getDelayPricePerHour() * delay;
            totalCost =+ delayCost;
        }
        log.info("Count bill method complete");
        return totalCost;
    }

}

package com.senchuk.carrentservice.service.impl;

import com.senchuk.carrentservice.model.TokenResponse;
import com.senchuk.carrentservice.service.RedisService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Collectors;

@Log4j
@Service
@RequiredArgsConstructor
public class RedisServiceImpl implements RedisService {

    @Value("${security.jwt.token.expiration.refresh}")
    private long refreshTokenExpirationTime;

    private final RedissonClient redissonClient;

    @Override
    public void save(final UUID id, final TokenResponse to) {
        var key = getKey(id.toString());
        var tokenStore = redissonClient.getScoredSortedSet(key);
        tokenStore.add(tokenStore.size(), to);
        tokenStore.expireAt(to.getRefreshTokenExpirationTime());
        log.info("Save token method completed");
    }

    @Override
    public void delete(final UUID id, final TokenResponse value) {
        var key = getKey(id.toString());
        redissonClient.getScoredSortedSet(key).remove(value);
        log.info("Delete token method completed");
    }

    @Override
    public void deleteAll(final UUID id) {
        var key = getKey(id.toString());
        redissonClient.getScoredSortedSet(key).delete();
        log.info("Delete all devices tokens method completed");
    }

    @Override
    public boolean checkToken(final UUID id, String token) {
        var key = getKey(id.toString());
        var tokenStore = redissonClient.getScoredSortedSet(key);
        var filteredTokens = tokenStore
                .stream()
                .map(TokenResponse.class::cast)
                .filter(tokenResponse -> tokenResponse.getAccessToken().equals(token) ||
                        tokenResponse.getRefreshToken().equals(token))
                .collect(Collectors.toList());
        log.info("Check token method completed");
        return !filteredTokens.isEmpty();
    }

    @Override
    public boolean isExists(final UUID id) {
        var key = getKey(id.toString());
        return redissonClient.getScoredSortedSet(key).isExists();
    }

    private String getKey(String id) {
        return String.format("user/%s/tokens", id);
    }
}


package com.senchuk.carrentservice.service.impl;

import com.senchuk.carrentservice.exception.EntityNotFoundException;
import com.senchuk.carrentservice.exception.InvalidAuthException;
import com.senchuk.carrentservice.exception.UserAlreadyRegisteredException;
import com.senchuk.carrentservice.model.AuthRequest;
import com.senchuk.carrentservice.model.TokenResponse;
import com.senchuk.carrentservice.security.JwtTokenProvider;
import com.senchuk.carrentservice.service.AuthService;
import com.senchuk.carrentservice.service.RedisService;
import com.senchuk.carrentservice.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.UUID;

import static com.senchuk.carrentservice.utils.SecurityUtils.getCurrentUser;


@Log4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserService userService;
    private final RedisService redisService;

    private final AuthenticationManager authenticationManager;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JwtTokenProvider jwtTokenProvider;


    @Override
    public TokenResponse signUp(final AuthRequest authRequest) {
        var isExist = userService.isExistByEmail(authRequest.getEmail());
        if (isExist) {
            throw new UserAlreadyRegisteredException(authRequest.getEmail());
        }
        var encodedPassword = bCryptPasswordEncoder.encode(authRequest.getPassword());
        authRequest.setPassword(encodedPassword);
        var savedUser = userService.create(authRequest);
        var tokenResponse = jwtTokenProvider.createTokens(savedUser.getEmail(),
                Collections.singletonList(savedUser.getScope().toString()));
        redisService.save(savedUser.getId(), tokenResponse);
        log.info("Sign up method completed");
        return tokenResponse;

    }

    @Override
    public TokenResponse signIn(final AuthRequest authRequest) {
        var user = userService.getByEmail(authRequest.getEmail());
        var authenticationToken = new UsernamePasswordAuthenticationToken(
                user.getEmail(), authRequest.getPassword()
        );
        try {
            authenticationManager.authenticate(authenticationToken);
        } catch (Exception e) {
            throw new EntityNotFoundException("Password is not correct");
        }
        var tokenResponse = jwtTokenProvider.createTokens(user.getEmail(),
                Collections.singletonList(user.getScope().toString()));
        redisService.save(user.getId(), tokenResponse);
        log.info("Sign in method completed");
        return tokenResponse;

    }

    @Override
    public TokenResponse refreshToken(final String refreshToken) {
        var isValid = jwtTokenProvider.validateToken(refreshToken);
        if (!isValid) {
            throw new InvalidAuthException("Invalid or expired authentication");
        }
        var email = jwtTokenProvider.getUsername(refreshToken);
        var scope = jwtTokenProvider.getScope(refreshToken);
        var listOfScope = Collections.singletonList(scope);
        var tokenResponse = jwtTokenProvider.createTokens(email, listOfScope);
        var user = userService.getByEmail(email);
        redisService.save(user.getId(), tokenResponse);
        log.info("Refresh token method completed");
        return tokenResponse;
    }

    @Override
    public void signOut(final TokenResponse tokenResponse) {
        var user = getCurrentUser();
        redisService.delete(user.getId(), tokenResponse);
        log.info("Sign out method completed");
    }

    @Override
    public void signOutAll() {
        var user = getCurrentUser();
        redisService.deleteAll(user.getId());
        log.info("Sign out from all devices method completed");
    }

    @Override
    public void signOutAllByUserId(final UUID userId) {
        redisService.deleteAll(userId);
        log.info(String.format("Sign out user with such id %s from all devices method completed", userId));
    }

}

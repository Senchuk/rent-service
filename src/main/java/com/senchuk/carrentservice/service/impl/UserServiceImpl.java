package com.senchuk.carrentservice.service.impl;

import com.senchuk.carrentservice.exception.EntityNotFoundException;
import com.senchuk.carrentservice.exception.InvalidRequestParamException;
import com.senchuk.carrentservice.model.AuthRequest;
import com.senchuk.carrentservice.model.User;
import com.senchuk.carrentservice.model.enums.UserScope;
import com.senchuk.carrentservice.repositoty.UserRepository;
import com.senchuk.carrentservice.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.senchuk.carrentservice.entity.enums.UserScopeEntity.ROLE_USER;
import static com.senchuk.carrentservice.mapper.UserMapper.USER_MAPPER;
import static com.senchuk.carrentservice.utils.SecurityUtils.getCurrentUser;

@Log4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User create(final AuthRequest authRequest) {
        var userEntity = USER_MAPPER.toEntity(authRequest);
        userEntity.setScope(ROLE_USER);
        var savedUserEntity = userRepository.save(userEntity);
        log.info("User create method completed");
        return USER_MAPPER.toShortModel(savedUserEntity);
    }

    @Override
    public List<User> getAll() {
        var users =  userRepository.findAll().stream().map(USER_MAPPER::toShortModel).collect(Collectors.toList());
        log.info("Get all users method completed");
        return users;
    }

    @Override
    public User getCurrent() {
        var id = getCurrentUser().getId();
        log.info("Get current user method completed");
        return getById(id);
    }

    @Override
    public User getById(final UUID id) {
        var userEntity = userRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException(String.format("User with such id %s is not exist", id));
        });
        log.info("Get user by id method completed");
        return USER_MAPPER.toShortModel(userEntity);
    }

    @Override
    public User getByEmail(final String email) {
        var userEntity = userRepository.findByEmail(email).orElseThrow(() -> {
            throw new EntityNotFoundException(String.format("User with such email %s is not exist", email));
        });
        log.info("Get user by email method completed");
        return USER_MAPPER.toModel(userEntity);
    }

    @Override
    public Boolean isExistByEmail(final String email) {
        var isExist =  userRepository.existsByEmail(email);
        log.info("Exist user by email method completed");
        return isExist;
    }

}

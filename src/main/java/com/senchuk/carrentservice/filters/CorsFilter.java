package com.senchuk.carrentservice.filters;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Objects;

import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

@Component
@Order(HIGHEST_PRECEDENCE)
class CorsFilter implements Filter {

    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {
        var httpServletResponse = (HttpServletResponse) response;
        var httpServletRequest = (HttpServletRequest) request;
        httpServletResponse.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
        httpServletResponse.setHeader("Access-Control-Max-Age", "3600");
        httpServletResponse.setHeader("Access-Control-Allow-Headers",
                "x-requested-with, authorization, Content-Type, " +
                        "Authorization, credential, X-XSRF-TOKEN, X-Total-Count"
        );
        httpServletResponse.setHeader("Access-Control-Expose-Headers", "X-Total-Count, X-Paging-PageSize");

        if (Objects.equals("OPTIONS".toLowerCase(), httpServletRequest.getMethod().toLowerCase())) {
            httpServletResponse.setStatus(SC_OK);
        } else {
            filterChain.doFilter(request, response);
        }
    }

}


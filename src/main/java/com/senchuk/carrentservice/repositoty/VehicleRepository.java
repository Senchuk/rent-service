package com.senchuk.carrentservice.repositoty;

import com.senchuk.carrentservice.entity.VehicleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface VehicleRepository extends JpaRepository<VehicleEntity, UUID> {

    @Query("SELECT vehicle " +
           "FROM VehicleEntity AS vehicle " +
           "WHERE vehicle.number = :number " +
           " AND vehicle.deletedAt IS NULL ")
    Optional<VehicleEntity> findByNumber(@Param("number") final String number);

    @Query("SELECT vehicle " +
           "FROM VehicleEntity AS vehicle " +
           "WHERE vehicle.id = :id " +
           " AND vehicle.deletedAt IS NULL")
    Optional<VehicleEntity> findById(@Param("id") final UUID id);

    @Query("SELECT vehicle " +
           "FROM VehicleEntity AS vehicle " +
           "WHERE vehicle.age = :now " +
           " AND vehicle.deletedAt IS NULL ")
    List<VehicleEntity> findAllByAge(@Param("now") final LocalDate now);

    @Query("SELECT vehicle " +
            "FROM VehicleEntity AS vehicle " +
            "WHERE vehicle.deletedAt IS NULL ")
    List<VehicleEntity> findAll();

}

package com.senchuk.carrentservice.repositoty;

import com.senchuk.carrentservice.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, UUID> {

    @Query("SELECT order FROM OrderEntity AS order WHERE order.user.id = :userId")
    List<OrderEntity> findByUserId(@Param("userId") UUID userId);

    @Query("SELECT order " +
            "FROM OrderEntity AS order " +
            "WHERE (:isPaid = TRUE AND order.isPaid IS NOT NULL) " +
            "OR (:isPaid = FALSE AND order.isPaid IS NULL)" +
            "OR (:isPaid IS NULL)")
    List<OrderEntity> findAll(@Param("isPaid") final Boolean isPaid);

    @Query("SELECT order " +
            "FROM OrderEntity AS order " +
            "WHERE order.vehicle.id = :vehicleId " +
            "AND ((:isPast = TRUE AND order.rentCompletionTime IS NOT NULL) " +
            "OR (:isPast = FALSE AND order.rentStartTime > :now AND order.rentCompletionTime IS NULL))")
    List<OrderEntity> findAllByVehicleId(@Param("vehicleId") final UUID vehicleId,
                                         @Param("isPast") final boolean isPast,
                                         @Param("now") final Instant now);

    @Query("select order from OrderEntity as order where order.vehicle.id = :vehicleId AND order.rentCompletionTime IS NOT NUll")
    List<OrderEntity> findPast(@Param("vehicleId") final UUID vehicleId);

}

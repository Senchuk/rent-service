package com.senchuk.carrentservice.repositoty;

import com.senchuk.carrentservice.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    @Query("SELECT user " +
            "FROM UserEntity AS user " +
            "WHERE user.email = :email " +
            " AND user.deletedAt IS NULL")
    Optional<UserEntity> findByEmail(@Param("email") final String email);

    @Query("SELECT user " +
            "FROM UserEntity AS user " +
            "WHERE user.id = :id " +
            " AND user.deletedAt IS NULL")
    Optional<UserEntity> findById(@Param("id") final UUID id);

    boolean existsByEmail(final String email);

}

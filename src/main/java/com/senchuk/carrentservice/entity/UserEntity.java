package com.senchuk.carrentservice.entity;

import com.senchuk.carrentservice.common.entity.AbstractEntity;
import com.senchuk.carrentservice.entity.enums.UserScopeEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.AUTO;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "`user`")
@EqualsAndHashCode(callSuper = true)
public class UserEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Enumerated(STRING)
    @Column(name = "scope", nullable = false)
    private UserScopeEntity scope;


}

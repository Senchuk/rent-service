package com.senchuk.carrentservice.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "`order`")
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name="vehicle_id", nullable = false)
    private VehicleEntity vehicle;

    @Column(name = "rent_start_time", nullable = false)
    private Instant rentStartTime;

    @Column(name = "duration", nullable = false)
    private Integer duration;

    @Column(name = "rent_completion_time")
    private Instant rentCompletionTime;

    @Column(name = "price_per_day")
    private Float pricePerDay;

    @Column(name = "delay_price_per_hour")
    private Float delayPricePerHour;

    @Column(name = "total_cost")
    private Float totalCost;

    @Column(name = "is_paid")
    private Boolean isPaid;

}

package com.senchuk.carrentservice.entity.enums;

public enum VehicleTypeEntity {

    AIRCRAFT,
    TRUCK,
    SEDAN,
    MINIVAN,
    BANTAM_CAR,
    BIKE,
    SCOOTER

}

package com.senchuk.carrentservice.entity.enums;

public enum QualityEntity {

    PERFECT,
    AVERAGE,
    BELOW_THE_AVERAGE

}

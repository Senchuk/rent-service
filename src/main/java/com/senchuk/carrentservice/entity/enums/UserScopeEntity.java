package com.senchuk.carrentservice.entity.enums;

public enum UserScopeEntity {

    ROLE_USER,
    ROLE_ADMIN

}

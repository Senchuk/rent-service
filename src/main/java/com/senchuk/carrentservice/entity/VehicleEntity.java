package com.senchuk.carrentservice.entity;

import com.senchuk.carrentservice.common.entity.AbstractEntity;
import com.senchuk.carrentservice.entity.enums.QualityEntity;
import com.senchuk.carrentservice.entity.enums.VehicleTypeEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.AUTO;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "vehicle")
@EqualsAndHashCode(callSuper = true)
public class VehicleEntity extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @Enumerated(STRING)
    @Column(name = "type", nullable = false)
    private VehicleTypeEntity type;

    @Column(name = "number", nullable = false)
    private String number;

    @Column(name = "age", nullable = false, updatable = false)
    private LocalDate age;

    @Enumerated(STRING)
    @Column(name = "quality", nullable = false)
    private QualityEntity quality;

    @Column(name = "description")
    private String description;

    @Column(name = "price_per_day", nullable = false)
    private Float pricePerDay;

    @Column(name = "delay_price_per_hour", nullable = false)
    private Float delayPricePerHour;

    @Column(name = "operational_discount", nullable = false)
    private Float operationalDiscount;

}

package com.senchuk.carrentservice.exception;

import com.senchuk.carrentservice.model.Error;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionHandling extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserAlreadyRegisteredException.class)
    public ResponseEntity<Object> userAlreadyRegisteredException(final UserAlreadyRegisteredException e,
                                                                 final WebRequest request) {
        var error = new Error();
        error.setMessage(e.getMessage());
        return handleExceptionInternal(e, error, new HttpHeaders(), e.getStatus(), request);
    }

    @ExceptionHandler(VehicleIsAlreadyExistException.class)
    public ResponseEntity<Object> vehicleIsAlreadyExistException(final VehicleIsAlreadyExistException e,
                                                                 final WebRequest request) {
        var error = new Error();
        error.setMessage(e.getMessage());
        return handleExceptionInternal(e, error, new HttpHeaders(), e.getStatus(), request);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> entityNotFountException(final EntityNotFoundException e,
                                                          final WebRequest request) {
        var error = new Error();
        error.setMessage(e.getMessage());
        return handleExceptionInternal(e, error, new HttpHeaders(), e.getStatus(), request);
    }

    @Override
    @ResponseBody
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
                                                                  final HttpHeaders headers,
                                                                  final HttpStatus status,
                                                                  final WebRequest request) {
        var errors = ex.getBindingResult().getFieldErrors().stream()
                .map(error ->  {
                    var unexpectedError = new Error();
                    unexpectedError.setMessage(error.getDefaultMessage());
                    return unexpectedError;
                })
                .collect(Collectors.toList());
        return new ResponseEntity<>(errors, headers, status);
    }

}

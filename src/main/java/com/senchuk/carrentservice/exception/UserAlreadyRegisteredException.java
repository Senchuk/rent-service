package com.senchuk.carrentservice.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

public class UserAlreadyRegisteredException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public UserAlreadyRegisteredException(final String email) {
        super(String.format("User with such email %s is already exist", email));
        status = UNPROCESSABLE_ENTITY;
    }


}

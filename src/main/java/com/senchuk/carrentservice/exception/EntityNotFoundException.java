package com.senchuk.carrentservice.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

public class EntityNotFoundException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public EntityNotFoundException(final String message) {
        super(message);
        status = NOT_FOUND;
    }


}

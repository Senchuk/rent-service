package com.senchuk.carrentservice.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

public class InvalidAuthException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public InvalidAuthException(final String message) {
        super(message);
        status = UNAUTHORIZED;
    }


}

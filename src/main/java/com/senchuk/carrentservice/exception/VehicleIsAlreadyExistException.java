package com.senchuk.carrentservice.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

public class VehicleIsAlreadyExistException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public VehicleIsAlreadyExistException(final String number) {
        super(String.format("Vehicle with such number %s is already exist", number));
        status = UNPROCESSABLE_ENTITY;
    }


}

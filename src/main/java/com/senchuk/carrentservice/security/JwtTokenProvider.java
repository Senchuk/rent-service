package com.senchuk.carrentservice.security;


import com.senchuk.carrentservice.model.Token;
import com.senchuk.carrentservice.model.TokenResponse;
import com.senchuk.carrentservice.service.RedisService;
import com.senchuk.carrentservice.service.UserService;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;


@Component
@RequiredArgsConstructor
public class JwtTokenProvider {

    @Value("${security.jwt.token.secret-key}")
    private String secretKey;
    @Value("${security.jwt.token.expiration.access}")
    private long accessTokenExpirationTime;
    @Value("${security.jwt.token.expiration.refresh}")
    private long refreshTokenExpirationTime;

    private static final String TOKEN_PREFIX = "Bearer ";

    private final UserService userService;
    private final RedisService redisService;


    public TokenResponse createTokens(final String username, final List<String> roles) {
        var accessToken = createToken(username, roles, accessTokenExpirationTime, secretKey);
        var refreshToken = createToken(username, roles, refreshTokenExpirationTime, secretKey);
        var tokenResponse = new TokenResponse();
        tokenResponse.setAccessToken(accessToken.getValue());
        tokenResponse.setAccessTokenExpirationTime(accessToken.getExpirationTime());
        tokenResponse.setRefreshToken(refreshToken.getValue());
        tokenResponse.setRefreshTokenExpirationTime(refreshToken.getExpirationTime());
        return tokenResponse;
    }

    public Authentication getAuthentication(final String token) {
        var email = getUsername(token);
        var userEntity = userService.getByEmail(email);
        var userDetails = new UserDetailsImpl();
        userDetails.setId(userEntity.getId());
        userDetails.setEmail(userEntity.getEmail());
        userDetails.setPassword(userEntity.getPassword());
        userDetails.setScope(userEntity.getScope());
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public String getUsername(final String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    public String getScope(final String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().get("roles").toString();
    }

    public String resolveToken(final HttpServletRequest req) {
        var bearerToken = req.getHeader(AUTHORIZATION);
        if (bearerToken != null && bearerToken.startsWith(TOKEN_PREFIX)) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public boolean validateToken(final String token) {
        var claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
        var email = claims.getSubject();
        var user = userService.getByEmail(email);
        var isExists = redisService.checkToken(user.getId(), token);
        if (!isExists) {
            return false;
        }
        return !claims.getExpiration().before(new Date());

    }

    private Token createToken(final String username, final List<String> roles,
                              final long tokenValidity, final String secretKey) {
        var claims = Jwts.claims();
        claims.put("roles", roles);
        var now = new Date();
        var validityTime = now.getTime() + tokenValidity;
        var validity = new Date(validityTime);
        var tokenValue = Jwts.builder()
                .setClaims(claims)
                .setSubject(username)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(HS256, secretKey)
                .compact();
        var token = new Token();
        token.setValue(tokenValue);
        token.setExpirationTime(validity);
        return token;
    }

}


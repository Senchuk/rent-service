package com.senchuk.carrentservice.security;

import com.senchuk.carrentservice.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetailsImpl loadUserByUsername(final String email) throws UsernameNotFoundException {
        var userEntity = userService.getByEmail(email);
        var userDetails = new UserDetailsImpl();
        userDetails.setId(userEntity.getId());
        userDetails.setEmail(userEntity.getEmail());
        userDetails.setPassword(userEntity.getPassword());
        userDetails.setScope(userEntity.getScope());
        return userDetails;
    }

}

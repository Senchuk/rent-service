package com.senchuk.carrentservice.controller;

import com.senchuk.carrentservice.model.User;
import com.senchuk.carrentservice.service.UserService;
import lombok.RequiredArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/users")
public class UserController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<User> getCurrentUser() {
        var user = userService.getCurrent();
        return new ResponseEntity<>(user, OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<User>> getAll() {
        var users = userService.getAll();
        return new ResponseEntity<>(users, OK);
    }

}

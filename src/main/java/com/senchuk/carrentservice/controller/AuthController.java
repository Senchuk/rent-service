package com.senchuk.carrentservice.controller;

import com.senchuk.carrentservice.model.AuthRequest;
import com.senchuk.carrentservice.model.TokenResponse;
import com.senchuk.carrentservice.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;


@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/auth")
public class AuthController {

    private final AuthService authService;

    @PostMapping("/signin")
    public ResponseEntity<TokenResponse> signIn(@Valid @RequestBody final AuthRequest authRequest) {
        var response = authService.signIn(authRequest);
        return new ResponseEntity<>(response, OK);
    }

    @PostMapping("/signup")
    public ResponseEntity<TokenResponse> signUp(@Valid @RequestBody final AuthRequest authRequest) {
        var response = authService.signUp(authRequest);
        return new ResponseEntity<>(response, CREATED);
    }

    @PostMapping("/refresh")
    public ResponseEntity<TokenResponse> refreshToken(@Valid @RequestBody final String refreshToken) {
        var response = authService.refreshToken(refreshToken);
        return new ResponseEntity<>(response, CREATED);
    }

    @PutMapping("/signout")
    public ResponseEntity<Void> signOut(@Valid @RequestBody final TokenResponse tokenResponse) {
        authService.signOut(tokenResponse);
        return new ResponseEntity<>(OK);
    }

    @PutMapping("/signout/all")
    public ResponseEntity<Void> signOutAll() {
        authService.signOutAll();
        return new ResponseEntity<>(OK);
    }

    @PutMapping("/signout/{userId}/all")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Void> signOutAllByUserUd(@PathVariable("userId") final UUID userId) {
        authService.signOutAllByUserId(userId);
        return new ResponseEntity<>(OK);
    }

}

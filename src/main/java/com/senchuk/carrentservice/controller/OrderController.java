package com.senchuk.carrentservice.controller;

import com.senchuk.carrentservice.model.*;
import com.senchuk.carrentservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/orders")
public class OrderController {

    private final OrderService orderService;

    @PostMapping
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Order> create(@RequestBody final OrderCreate orderCreate) {
        var savedOrder = orderService.create(orderCreate);
        return new ResponseEntity<>(savedOrder, CREATED);
    }

    @GetMapping
    public ResponseEntity<List<Order>> getAllByCurrentUser() {
        var orders = orderService.getAllByCurrentUser();
        return new ResponseEntity<>(orders, OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Order> getById(@Valid @PathVariable("id") final UUID id) {
        var order = orderService.getById(id);
        return new ResponseEntity<>(order, OK);
    }

    @GetMapping("/{id}/bill")
    public ResponseEntity<OrderBill> getBillById(@Valid @PathVariable("id") final UUID id) {
        var orderBill = orderService.getBillById(id);
        return new ResponseEntity<>(orderBill, OK);
    }

    @PutMapping("/{id}/end")
    public ResponseEntity<Order> endRent(@Valid @PathVariable("id") final UUID id) {
        var order = orderService.endRent(id);
        return new ResponseEntity<>(order, OK);
    }

    @GetMapping("/{id}/all")
    public ResponseEntity<List<Order>> getAllByVehicleId(@Valid @PathVariable("id") final UUID vehicleId,
                                                         @Valid @RequestParam("isPast") final Boolean isPast) {
        var orders = orderService.getAllByVehicleId(vehicleId, isPast);
        return new ResponseEntity<>(orders, OK);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Order>> getAllByIsPaid(@Valid @RequestParam("isPaid") @Nullable final Boolean isPaid) {
        var orders = orderService.getAllByIsPaid(isPaid);
        return new ResponseEntity<>(orders, OK);
    }

    @PutMapping("/{id}/pay")
    public ResponseEntity<Order> pay(@Valid @PathVariable("id") final UUID id) {
        var order = orderService.pay(id);
        return new ResponseEntity<>(order, OK);
    }


}

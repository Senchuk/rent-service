package com.senchuk.carrentservice.controller;

import com.senchuk.carrentservice.model.Vehicle;
import com.senchuk.carrentservice.model.VehicleUpdate;
import com.senchuk.carrentservice.service.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;

    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Vehicle> create(@Valid @RequestBody final Vehicle vehicle) {
        var savedVehicle = vehicleService.create(vehicle);
        return new ResponseEntity<>(savedVehicle, CREATED);
    }

    @PutMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Vehicle> update(@Valid @RequestBody final VehicleUpdate vehicleUpdate) {
        var updatedVehicle = vehicleService.update(vehicleUpdate);
        return new ResponseEntity<>(updatedVehicle, OK);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Vehicle> delete(@PathVariable("id") final UUID id) {
        vehicleService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

    @GetMapping
    public ResponseEntity<List<Vehicle>> getAll() {
        var vehicles = vehicleService.getAll();
        return new ResponseEntity<>(vehicles, OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Vehicle> getById(@Valid @PathVariable("id") final UUID id) {
        var vehicles = vehicleService.getById(id);
        return new ResponseEntity<>(vehicles, OK);
    }

}

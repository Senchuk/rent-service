package com.senchuk.carrentservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderPayment {

    private UUID orderId;

    private Float amount;

}

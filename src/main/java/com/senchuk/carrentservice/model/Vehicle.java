package com.senchuk.carrentservice.model;

import com.senchuk.carrentservice.common.model.AbstractModel;
import com.senchuk.carrentservice.model.enums.Quality;
import com.senchuk.carrentservice.model.enums.VehicleType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Vehicle extends AbstractModel {

    private UUID id;

    @NotNull(message = "Type cannot be null")
    @NotEmpty(message = "Invalid type id input")
    private VehicleType type;

    @NotNull(message = "Number cannot be null")
    @NotEmpty(message = "Invalid number id input")
    private String number;

    @NotNull(message = "Age cannot be null")
    @NotEmpty(message = "Invalid age id input")
    private LocalDate age;

    @NotNull(message = "Quality cannot be null")
    @NotEmpty(message = "Invalid quality id input")
    private Quality quality;

    @NotNull(message = "Description cannot be null")
    @NotEmpty(message = "Invalid description id input")
    private String description;

    @NotNull(message = "Price cannot be null")
    @NotEmpty(message = "Invalid price id input")
    private Float pricePerDay;

    @NotNull(message = "Delay price cannot be null")
    @NotEmpty(message = "Invalid delay price id input")
    private Float delayPricePerHour;

    @NotNull(message = "Operational discount cannot be null")
    @NotEmpty(message = "Invalid operational discount id input")
    private Float operationalDiscount;

}

package com.senchuk.carrentservice.model;

import com.senchuk.carrentservice.model.enums.Quality;
import com.senchuk.carrentservice.model.enums.VehicleType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class VehicleUpdate {

    private UUID id;

    @NotNull(message = "Quality cannot be null")
    @NotEmpty(message = "Invalid quality id input")
    private Quality quality;

    @NotNull(message = "Description cannot be null")
    @NotEmpty(message = "Invalid description id input")
    private String description;

    @NotNull(message = "Price cannot be null")
    @NotEmpty(message = "Invalid price id input")
    private Float pricePerDay;

    @NotNull(message = "Delay price cannot be null")
    @NotEmpty(message = "Invalid delay price id input")
    private Float delayPricePerHour;

    @NotNull(message = "Operational discount cannot be null")
    @NotEmpty(message = "Invalid operational discount id input")
    private Float operationalDiscount;

}

package com.senchuk.carrentservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderCreate {

    @NotNull(message = "Vehicle id cannot be null")
    @NotEmpty(message = "Vehicle id cannot be empty")
    private UUID vehicleId;

    @NotNull(message = "Rent start time cannot be null")
    @NotEmpty(message = "Rent start time cannot be empty")
    private Instant rentStartTime;

    @NotNull(message = "Duration cannot be null")
    @NotEmpty(message = "Duration cannot be null")
    private Integer duration;

}

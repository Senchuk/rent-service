package com.senchuk.carrentservice.model;

import com.senchuk.carrentservice.common.model.AbstractModel;
import com.senchuk.carrentservice.model.enums.UserScope;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class User extends AbstractModel {

    private UUID id;

    @Email
    private String email;

    @Pattern(regexp = "(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[0-9a-zA-Z]{5,}")
    private String password;

    private UserScope scope;

}

package com.senchuk.carrentservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    private UUID id;

    private UUID userId;

    private UUID vehicleId;

    private Instant rentStartTime;

    private Integer duration;

    private Instant rentCompletionTime;

    private Float pricePerDay;

    private Float delayPricePerHour;

    private Float totalCost;

    private Boolean isPaid;

}

package com.senchuk.carrentservice.model;

import lombok.Data;

@Data
public class Error {

    private String message;

}

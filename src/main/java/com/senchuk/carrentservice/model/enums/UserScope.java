package com.senchuk.carrentservice.model.enums;

public enum UserScope {

    ROLE_USER,
    ROLE_ADMIN

}

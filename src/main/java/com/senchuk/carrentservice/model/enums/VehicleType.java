package com.senchuk.carrentservice.model.enums;

public enum VehicleType {

    AIRCRAFT,
    TRUCK,
    SEDAN,
    MINIVAN,
    BANTAM_CAR,
    BIKE,
    SCOOTER

}

package com.senchuk.carrentservice.model.enums;

public enum Quality {

    PERFECT,
    AVERAGE,
    BELOW_THE_AVERAGE

}

package com.senchuk.carrentservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthRequest {

    @Email(message = "Invalid email input")
    @NotNull(message = "Email cannot be null")
    @NotEmpty(message = "Invalid email input")
    private String email;

    @NotNull(message = "Password cannot be null")
    @NotEmpty(message = "Invalid password input")
    @Pattern(regexp = "[A-Za-z0-9]{5,}", message = "Invalid password input")
    private String password;

}

package com.senchuk.carrentservice.mapper;

import com.senchuk.carrentservice.entity.VehicleEntity;
import com.senchuk.carrentservice.model.Vehicle;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface VehicleMapper {

    VehicleMapper VEHICLE_MAPPER = Mappers.getMapper(VehicleMapper.class);

    VehicleEntity toEntity(Vehicle vehicle);

    Vehicle toModel(VehicleEntity vehicleEntity);

}

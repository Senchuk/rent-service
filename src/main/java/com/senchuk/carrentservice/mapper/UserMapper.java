package com.senchuk.carrentservice.mapper;

import com.senchuk.carrentservice.entity.UserEntity;
import com.senchuk.carrentservice.model.AuthRequest;
import com.senchuk.carrentservice.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    UserEntity toEntity(final User user);

    UserEntity toEntity(final AuthRequest authRequest);

    User toModel(final UserEntity userEntity);

    @Mapping(target = "password", ignore = true)
    User toShortModel(final UserEntity userEntity);

//userEntity    @ValueMapping(target = "ROLE_SPONSOR", source = "SPONSOR")
//    @ValueMapping(target = "ROLE_RESEARCHER", source = "RESEARCHER")
//    @ValueMapping(target = "ROLE_STORAGE_MANAGER", source = "STORAGE_MANAGER")
//    @ValueMapping(target = "ROLE_MAIN_STORAGE_MANAGER", source = "MAIN_STORAGE_MANAGER")
//    User toEntity(UserScope scope);

}

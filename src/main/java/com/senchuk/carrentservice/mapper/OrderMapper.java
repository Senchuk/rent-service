package com.senchuk.carrentservice.mapper;

import com.senchuk.carrentservice.entity.OrderEntity;
import com.senchuk.carrentservice.model.Order;
import com.senchuk.carrentservice.model.OrderBill;
import com.senchuk.carrentservice.model.OrderCreate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderMapper {

    OrderMapper ORDER_MAPPER = Mappers.getMapper(OrderMapper.class);

    OrderEntity toEntity(final Order order);

    OrderEntity toEntity(final OrderCreate orderCreate);

    OrderBill toOrderBill(final Order order);

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "vehicle.id", target = "vehicleId")
    Order toModel(final OrderEntity orderEntity);

}

CREATE TYPE USER_SCOPE AS ENUM ('ROLE_USER', 'ROLE_ADMIN');

CREATE TABLE "user" (
   id UUID PRIMARY KEY,
   email VARCHAR (55) UNIQUE NOT NULL,
   password VARCHAR (255) NOT NULL,
   scope VARCHAR(55) NOT NULL,
   created_at timestamp NOT NULL,
   updated_at timestamp,
   deleted_at timestamp
);

CREATE TYPE VEHICLE_TYPE AS ENUM ('AIRCRAFT', 'TRUCK', 'SEDAN', 'MINIVAN', 'BANTAM_CAR', 'BIKE', 'SCOOTER');

CREATE TYPE QUALITY AS ENUM ('PERFECT', 'AVERAGE', 'BELOW_THE_AVERAGE');

CREATE TABLE vehicle (
    id UUID PRIMARY KEY,
    type VARCHAR(55) NOT NULL,
    number VARCHAR (55) UNIQUE NOT NULL,
    age DATE NOT NULL,
    quality VARCHAR(55) NOT NULL,
    description VARCHAR (255),
    price_per_day FLOAT NOT NULL,
    delay_price_per_hour FLOAT NOT NULL,
    operational_discount FLOAT NOT NULL,
    created_at timestamp NOT NULL,
    updated_at timestamp,
    deleted_at timestamp
);

CREATE TABLE "order" (
    id UUID PRIMARY KEY,
    user_id UUID NOT NULL,
    vehicle_id UUID NOT NULL,
    price_per_day FLOAT NOT NULL,
    delay_price_per_hour FLOAT NOT NULL,
    rent_start_time TIMESTAMP NOT NULL,
    duration INT NOT NULL,
    rent_completion_time TIMESTAMP,
    total_cost FLOAT,
    is_paid BOOLEAN DEFAULT FALSE,
    FOREIGN KEY (user_id) REFERENCES "user" (id),
    FOREIGN KEY (vehicle_id) REFERENCES vehicle (id)
);


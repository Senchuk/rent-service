package com.senchuk.carrentservice.integration.authControllerTest;

import com.senchuk.carrentservice.integration.AbstractIntegrationTest;
import com.senchuk.carrentservice.model.TokenResponse;
import org.junit.Test;

import static com.senchuk.carrentservice.utils.TestUtil.createAuthRequest;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testcontainers.shaded.org.apache.http.HttpHeaders.AUTHORIZATION;

public class AuthControllerTest extends AbstractIntegrationTest {

    private static final String AUTH_URL = BASE_URL + "/auth";

    //region signUp tests

    @Test
    public void signUpUser_happyPath() throws Exception {
        //GIVEN
        var authRequest = createAuthRequest( "test@mail.com", "Password123");

        //WHEN
        var mvcResult =
                mockMvc.perform(post(AUTH_URL + "/signup")
                        .content(objectMapper.writeValueAsString(authRequest))
                        .contentType(APPLICATION_JSON))
                        .andExpect(status().isCreated())
                        .andReturn();

        var response = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), TokenResponse.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(response.getAccessToken()).isNotNull();
            softly.assertThat(response.getAccessTokenExpirationTime()).isNotNull();
            softly.assertThat(response.getRefreshToken()).isNotNull();
            softly.assertThat(response.getRefreshTokenExpirationTime()).isNotNull();
        });
    }

    @Test
    public void signUpUserWithEmailThatBelongsToAnotherUser() throws Exception {
        //GIVEN
        var firstAuthRequest = createAuthRequest("test@mail.com", "Password123");
        singUp(firstAuthRequest);
        var secondAuthRequest = createAuthRequest("test@mail.com", "Password123");

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(secondAuthRequest))
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isUnprocessableEntity());
    }

    //endregion

    //region signIn tests

    @Test
    public void signInUser_happyPath() throws Exception {
        //GIVEN
        var authRequestForSignUp = createAuthRequest("test@mail.com", "Password123");
        singUp(authRequestForSignUp);
        var authRequestForSignIn = createAuthRequest("test@mail.com", "Password123");

        //WHEN
        var mvcResult = mockMvc.perform(post(AUTH_URL + "/signin")
                .content(objectMapper.writeValueAsString(authRequestForSignIn))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        var tokenResponse = objectMapper.readValue(
                mvcResult.getResponse().getContentAsByteArray(), TokenResponse.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(tokenResponse.getAccessToken()).isNotNull();
            softly.assertThat(tokenResponse.getAccessTokenExpirationTime()).isNotNull();
            softly.assertThat(tokenResponse.getRefreshToken()).isNotNull();
            softly.assertThat(tokenResponse.getRefreshTokenExpirationTime()).isNotNull();
        });
    }

    @Test
    public void signInUserWithEmailThatNotExists() throws Exception {
        //GIVEN
        var userSignInRequest = createAuthRequest("test1@mail.com", "Password123");

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/signin")
                .content(objectMapper.writeValueAsString(userSignInRequest))
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    @Test
    public void signInUserWithIncorrectPassword() throws Exception {
        //GIVEN
        var authRequestForSignUp = createAuthRequest("test@mail.com", "Password123");
        singUp(authRequestForSignUp);
        var authRequestForSignIn = createAuthRequest("test@mail.com", "Password111");

        //WHEN
        var resultActions = mockMvc.perform(post(AUTH_URL + "/signin")
                .content(objectMapper.writeValueAsString(authRequestForSignIn))
                .contentType(APPLICATION_JSON));

        //THEN
        resultActions.andExpect(status().isNotFound());
    }

    //endregion

    //region signOut tests

    @Test
    public void signOutUser_happyPath() throws Exception {
        //GIVEN
        var authRequest = createAuthRequest("test@mail.com", "Password123");
        var tokenResponse = singUp(authRequest);
        var currentUser = getCurrent(tokenResponse.getAccessToken());

        //WHEN
        mockMvc.perform(put(AUTH_URL + "/signout")
                .header(AUTHORIZATION, AUTH_HEADER + tokenResponse.getAccessToken())
                .content(objectMapper.writeValueAsString(tokenResponse))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());

        //THEN
        assertSoftly(softly -> softly.assertThat(redisService.isExists(currentUser.getId())).isFalse());
    }

    //endregion

    //region signOut tests

    @Test
    public void signOutAll_happyPath() throws Exception {
        //GIVEN
        var authRequest = createAuthRequest("test@mail.com", "Password123");
        var tokenResponse = singUp(authRequest);
        var currentUser = getCurrent(tokenResponse.getAccessToken());

        //WHEN
        mockMvc.perform(put(AUTH_URL + "/signout/all")
                .header(AUTHORIZATION, AUTH_HEADER + tokenResponse.getAccessToken())
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());

        //THEN
        assertSoftly(softly -> softly.assertThat(redisService.isExists(currentUser.getId())).isFalse());
    }

    //endregion

}


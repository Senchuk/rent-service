package com.senchuk.carrentservice.integration.initializer;

import org.junit.ClassRule;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.util.UriComponentsBuilder;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

import javax.validation.constraints.NotNull;

public class RedisInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final int REDIS_PORT = 6379;

    private static final String REDIS_PASSWORD_SETTINGS = "yes";

    @ClassRule
    private static final GenericContainer REDIS =
            new GenericContainer("redis:latest")
                    .withEnv("POSTGRES_DB", REDIS_PASSWORD_SETTINGS)
                    .waitingFor(Wait.forListeningPort())
                    .withExposedPorts(REDIS_PORT);

    @Override
    public void initialize(@NotNull final ConfigurableApplicationContext applicationContext) {
        applyProperties(applicationContext);
    }

    private void applyProperties(final ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
                "redis.url:" + getUrl()
        ).applyTo(applicationContext);
    }

    private String getUrl() {
        return "redis:" + UriComponentsBuilder.newInstance().host(REDIS.getContainerIpAddress())
                .port(REDIS.getMappedPort(REDIS_PORT)).toUriString();
    }

    static {
        REDIS.start();
    }

}


package com.senchuk.carrentservice.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.senchuk.carrentservice.integration.initializer.PostgresInitializer;
import com.senchuk.carrentservice.integration.initializer.RedisInitializer;
import com.senchuk.carrentservice.model.AuthRequest;
import com.senchuk.carrentservice.model.TokenResponse;
import com.senchuk.carrentservice.model.User;
import com.senchuk.carrentservice.repositoty.OrderRepository;
import com.senchuk.carrentservice.repositoty.UserRepository;
import com.senchuk.carrentservice.repositoty.VehicleRepository;
import com.senchuk.carrentservice.service.RedisService;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testcontainers.shaded.org.apache.http.HttpHeaders.AUTHORIZATION;

@AutoConfigureMockMvc
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@ContextConfiguration(initializers = {
        RedisInitializer.class,
        PostgresInitializer.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractIntegrationTest {

    protected static final String BASE_URL = "/api/v1";
    protected static final String AUTH_HEADER = "Bearer ";


    @Autowired
    private UserRepository userRepository;

    @Autowired
    protected RedisService redisService;

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;

    protected TokenResponse singUp(final AuthRequest authRequest) throws Exception {
        var mvcResult = mockMvc.perform(post(BASE_URL + "/auth/signup")
                .content(objectMapper.writeValueAsString(authRequest))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), TokenResponse.class);
    }

    protected User getCurrent(final String accessToken) throws Exception {
        var mvcResult = mockMvc.perform(get(BASE_URL + "/users")
                .header(AUTHORIZATION, AUTH_HEADER + accessToken))
                .andExpect(status().isOk())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), User.class);
    }

    @Before
    public void clearDatabase() {
        userRepository.deleteAll();
    }


}

package com.senchuk.carrentservice.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.senchuk.carrentservice.CarRentServiceApplication;
import com.senchuk.carrentservice.controller.AuthController;
import com.senchuk.carrentservice.exception.ExceptionHandling;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@AutoConfigureMockMvc
@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest(classes = CarRentServiceApplication.class)
public abstract class AbstractUnitTest {

    protected static final String BASE_URL = "/api/v1";

    @InjectMocks
    private AuthController authController;

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.objectMapper = new ObjectMapper();
        this.mockMvc = MockMvcBuilders.standaloneSetup(authController)
                .setControllerAdvice(new ExceptionHandling())
                .build();
    }

}


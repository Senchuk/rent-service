package com.senchuk.carrentservice.unit.authControllerTest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.senchuk.carrentservice.model.TokenResponse;
import com.senchuk.carrentservice.service.AuthService;
import com.senchuk.carrentservice.unit.AbstractUnitTest;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import static com.senchuk.carrentservice.utils.TestUtil.createAuthRequest;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testcontainers.shaded.javax.ws.rs.core.MediaType.APPLICATION_JSON;

public class AuthControllerTest extends AbstractUnitTest {

    private static final String AUTH_URL = BASE_URL + "/auth";

    @Mock
    private AuthService authService;

    //region signUp tests

    @Test
    public void signUpUser_happyPath() throws Exception {
        //GIVEN
        var request = createAuthRequest("test@mail.com", "Password123");
        given(authService.signUp(request)).willReturn(new TokenResponse());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andReturn();

        //THEN
        assertThat(response.getResponse().getStatus()).isEqualTo(201);
    }


    @Test
    public void signUpUserWhenEmailIsInvalid() throws Exception {
        //GIVEN
        var request = createAuthRequest("test", "Password123");
        given(authService.signUp(request)).willReturn(new TokenResponse());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<Error> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo("Invalid email input");
        });
    }
    

    @Test
    public void signUpUserWhenPasswordIsInvalid() throws Exception {
        //GIVEN
        //GIVEN
        var request = createAuthRequest("test@mail.com", "test");
        given(authService.signUp(request)).willReturn(new TokenResponse());

        //WHEN
        var response = mockMvc.perform(post(AUTH_URL + "/signup")
                .content(objectMapper.writeValueAsString(request))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        final List<Error> errors = objectMapper.readValue(response.getResponse().getContentAsByteArray(),
                new TypeReference<>() {
                });

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(errors.size()).isEqualTo(1);
            softly.assertThat(errors.get(0).getMessage()).isEqualTo("Invalid password input");
        });
    }

    //endregion

}


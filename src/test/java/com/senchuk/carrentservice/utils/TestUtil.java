package com.senchuk.carrentservice.utils;

import com.senchuk.carrentservice.model.AuthRequest;

public class TestUtil {

    public static AuthRequest createAuthRequest(final String email, final String password) {
        var authRequest = new AuthRequest();
        authRequest.setEmail(email);
        authRequest.setPassword(password);
        return authRequest;
    }

}

